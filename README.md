# **Helptec**

#### Status Project
- **Developmen**t

#### About project
The general idea is a Goal Tracker. The point here, is not to have a great goal tracker(though that might happen) but to expand knowledge in many progeamming concepts, patterns and frameworks.
#### Contribute
- Create issues to find bugs, new ideas or new features;

#### Technologies
- Java;
- H2 Databse
- JPA;
- Lombok
- Spring
- Mockito

#### Patterns
- Factory;
- Repository;
- Package;
- Single responsability;
