/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opensoftware.helptec.valueobjects;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.*;

/**
 * @author Hugo
 * @date 2/jul/2018
 */
@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Name implements Serializable {
	@NonNull
	private String name;
	
}
