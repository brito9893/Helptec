package com.opensoftware.helptec.valueobjects;

import javax.persistence.Embeddable;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Email {
	@NonNull
	private String email;
	
}
