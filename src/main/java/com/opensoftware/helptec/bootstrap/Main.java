/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.opensoftware.helptec.bootstrap;

import com.opensoftware.helptec.domain.Goal;
import com.opensoftware.helptec.repository.GoalRepository;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Hugo
 */
public class Main {
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		GoalRepository gr = new GoalRepository();
		gr.save(Goal.builder().description("New Goal").start(Calendar.getInstance().getTime()).end(new Date(2019, 3, 1)).build());
	}
	
}
