package com.opensoftware.helptec.ui;

import java.io.IOException;
import java.util.Objects;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CreateGoalApplication extends Application {
	
	CreateGoalApplication() {
		try {
			start(new Stage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws IOException {
		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/createGoal.fxml")));
		
		stage.setScene(new Scene(Objects.requireNonNull(root)));
		stage.setResizable(false);
		stage.showAndWait();
	}
	
}
