package com.opensoftware.helptec.ui;

import com.opensoftware.helptec.domain.Goal;
import com.opensoftware.helptec.repository.GoalRepository;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class CreateGoalController {
	public DatePicker startDateDatePicker;
	public DatePicker endDateDatePicker;
	public ProgressBar progressBar;
	public TextField descriptionField;
	public TextArea contextTextArea;
	public Button saveButton;
	public Button cancelButton;
	public TextArea motivationTextArea;
	
	private GoalRepository goalRepository = new GoalRepository();
	
	public void saveButtonOnClick(ActionEvent actionEvent) {
		if (startDateDatePicker.getValue() != null && endDateDatePicker.getValue() != null && descriptionField.getText() != null) {
			LocalDate localDate = startDateDatePicker.getValue();
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date start = Date.from(instant);
			
			localDate = endDateDatePicker.getValue();
			instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date end = Date.from(instant);
			
			
			if (end.after(start) && start.after(Calendar.getInstance().getTime()) || start.equals(Calendar.getInstance().getTime())) {
				goalRepository.save(
						Goal.builder()
								.description(descriptionField.getText())
								.start(start)
								.end(end)
								.context(contextTextArea.getText())
								.motivation(motivationTextArea.getText())
								.notes(new ArrayList<>())
								.build()
				);
				close();
			} else {
				new Alert(AlertType.ERROR, "Start date must be b4 end date." +
						                           "\nOr" +
						                           "\nStart date can't be set before current date!",
						ButtonType.CLOSE).showAndWait();
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR, "Some information is missing, do a double check and comeback.", ButtonType.CLOSE);
			alert.showAndWait();
		}
		
		
	}
	
	public void cancelButtonOnClick(ActionEvent actionEvent) {
		close();
	}
	
	private void close() {
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}
}
