package com.opensoftware.helptec.ui;

import com.opensoftware.helptec.domain.Goal;
import com.opensoftware.helptec.repository.GoalRepository;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

public class HomepageController {
	public Button seeGoalsButton;
	public Button createGoalButton;
	
	private GoalRepository goalRepository = new GoalRepository();
	
	public void showGoalsOnClick(ActionEvent actionEvent) {
		List<Goal> goals = goalRepository.findAll();
		for (Goal goal : goals) {
			System.out.println(goal);
		}
	}
	
	public void createGoalOnClick(ActionEvent actionEvent) {
		new CreateGoalApplication();
	}
}
