package com.opensoftware.helptec.repository;

import com.opensoftware.helptec.domain.Goal;
import com.opensoftware.helptec.persistence.PersistenceRepository;

public class GoalRepository extends PersistenceRepository<Goal, Long> {
}
